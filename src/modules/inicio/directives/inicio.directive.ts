import { Directive, Input } from '@angular/core';

@Directive({
    selector: '[sbInicio]',
})
export class InicioDirective {
    @Input() param!: string;

    constructor() {}
}
