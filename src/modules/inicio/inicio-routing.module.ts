/* tslint:disable: ordered-imports*/
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

/* Module */
import { InicioModule } from './inicio.module';

/* Containers */
import * as inicioContainers from './containers';

/* Guards */
import * as inicioGuards from './guards';
import { SBRouteData } from '@modules/navigation/models';

/* Routes */
export const ROUTES: Routes = [
    {
        path: '',
        data: {
            title: 'inicio',
        } as SBRouteData,
        canActivate: [],
        component: inicioContainers.PaginaPrincipalComponent,
    },
];


@NgModule({
    imports: [InicioModule, RouterModule.forChild(ROUTES)],
    exports: [RouterModule],
})
export class InicioRoutingModule {}
