import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from '@modules/login/services';

@Component({
    selector: 'sb-pagina-principal',
    changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './pagina-principal.component.html',
    styleUrls: ['pagina-principal.component.scss'],
})
export class PaginaPrincipalComponent implements OnInit {
    constructor( private router:Router) {}
    controlVistas: string = 'imagen';
    ngOnInit() {}
    login(){
        this.router.navigate(['/login']);
    }
    registro(){
        this.router.navigate(['/registro/general']);
    }


}

