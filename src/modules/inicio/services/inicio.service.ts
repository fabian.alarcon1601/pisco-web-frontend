import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

@Injectable()
export class InicioService {
    constructor() {}

    getInicio$(): Observable<{}> {
        return of({});
    }

}
