import { TestBed } from '@angular/core/testing';

import { InicioService } from './inicio.service';

describe('InicioService', () => {
    let inicioService: InicioService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [InicioService],
        });
        inicioService = TestBed.inject(InicioService);
    });

    describe('getInicio$', () => {
        it('should return Observable<Inicio>', () => {
            expect(inicioService).toBeDefined();
        });
    });
});
