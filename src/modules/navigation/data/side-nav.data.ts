import { isNgTemplate } from '@angular/compiler';
import { SideNavItems, SideNavSection } from '@modules/navigation/models';

export const sideNavSections: SideNavSection[] = items();

export const sideNavItems: SideNavItems = {
    buscarProfesional: {
        icon: 'search',
        text: 'Buscar profesional',
        link: '/agendar/hora/buscar/profesional',
    },
    fichaClinica: {
        icon: 'clipboard-list',
        text: 'Fichas pacientes',
        link: '/fichas/clinicas/ficha/clinica',
    },
    solicitudes: {
        icon: 'list-alt',
        text: 'Solicitudes pendientes',
        link: '/solicitudes/psicologos/listado/solicitudes',
    },
    solicitudesRechazadas: {
        icon: 'list-alt',
        text: 'Solicitudes rechazadas',
        link: '/solicitudes/psicologos/solicitudes/rechazadas',
    },
    solicitudesAprobadas: {
        icon: 'list-alt',
        text: 'Solicitudes aprobadas',
        link: '/solicitudes/psicologos/solicitudes/aprobadas',
    },
    horario: {
        icon: 'list-alt',
        text: 'Horario ',
        link: '/horario/carga/horario',
    },


};

function items(){
    let localUser = JSON.parse(localStorage.getItem('usuario'));
    let arrayItems=[];
    if (localUser.rol.nombre == 'admin') {
        arrayItems=[
            {
                text: 'Solicitudes Psicólogos',
                items: ['solicitudes', 'solicitudesAprobadas', 'solicitudesRechazadas'],
            },
        ]
    }else{
        if(localUser.rol.nombre == 'profesional'){
            arrayItems=[
            {
                text: 'Paciente',
                items: ['fichaClinica'],
            },
            {
                text: 'Psicologo',
                items: ['horario'],
            },
        ]
        }else{
            if(localUser.rol.nombre == 'paciente'){
                arrayItems=[
                    {
                        text: 'Reserva',
                        items: ['buscarProfesional'],
                    },
                ]
            }
        }
    }
    return arrayItems;
}