import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { url } from "../../../environments/environment";

const _expand$ = new Subject<string[]>();

const expandedTable: {
    [index: string]: boolean;
} = {};

@Injectable()
export class SideNavService {
    private headers = new HttpHeaders(
        {  'Content-Type':  'application/json' }
      );
    constructor(private http:HttpClient) {}

    get expand$() {
        return _expand$;
    }

    isExpanded(hash: string): boolean {
        if (expandedTable[hash]) {
            return true;
        }
        return false;
    }

    setExpanded(hash: string, expanded: boolean) {
        expandedTable[hash] = expanded;
    }

    logout(data){
        const option = {headers:this.headers};
        return this.http.post(url + '/usuario/logout',data, option);
    }


}
