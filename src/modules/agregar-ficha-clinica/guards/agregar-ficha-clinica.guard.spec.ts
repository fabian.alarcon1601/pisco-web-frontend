import { TestBed } from '@angular/core/testing';

import { AgregarFichaClinicaGuard } from './agregar-ficha-clinica.guard';

describe('AgregarFichaClinica Guards', () => {
    let agregarFichaClinicaGuard: AgregarFichaClinicaGuard;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [AgregarFichaClinicaGuard],
        });
        agregarFichaClinicaGuard = TestBed.inject(AgregarFichaClinicaGuard);
    });

    describe('canActivate', () => {
        it('should return an Observable<boolean>', () => {
            agregarFichaClinicaGuard.canActivate().subscribe(response => {
                expect(response).toEqual(true);
            });
        });
    });

});
