import { TestBed } from '@angular/core/testing';

import { AgregarFichaClinicaService } from './agregar-ficha-clinica.service';

describe('AgregarFichaClinicaService', () => {
    let agregarFichaClinicaService: AgregarFichaClinicaService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [AgregarFichaClinicaService],
        });
        agregarFichaClinicaService = TestBed.inject(AgregarFichaClinicaService);
    });

    describe('getAgregarFichaClinica$', () => {
        it('should return Observable<AgregarFichaClinica>', () => {
            expect(agregarFichaClinicaService).toBeDefined();
        });
    });
});
