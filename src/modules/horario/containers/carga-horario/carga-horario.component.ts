import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

@Component({
    selector: 'sb-carga-horario-container',
    changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './carga-horario.component.html',
    styleUrls: ['carga-horario.component.scss'],
})
export class CargaHorarioComponent implements OnInit {
    constructor() {}
    ngOnInit() {}
}
