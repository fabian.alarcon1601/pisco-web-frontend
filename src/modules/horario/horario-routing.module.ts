/* tslint:disable: ordered-imports*/
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

/* Module */
import { HorarioModule } from './horario.module';

/* Containers */
import * as cargaHorarioContainers from './containers';

/* Guards */
import * as horarioGuards from './guards';
import { SBRouteData } from '@modules/navigation/models';
import { CargaHorarioComponent } from './containers';

/* Routes */
export const ROUTES: Routes = [
    {
        path: 'carga/horario',
        data: {
            title: 'Carga de horario',
        } as SBRouteData,
        canActivate: [],
        component: cargaHorarioContainers.CargaHorarioComponent,
    },
];

@NgModule({
    imports: [HorarioModule, RouterModule.forChild(ROUTES)],
    exports: [RouterModule],
})
export class HorarioRoutingModule {}
