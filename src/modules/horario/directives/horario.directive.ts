import { Directive, Input } from '@angular/core';

@Directive({
    selector: '[sbHorario]',
})
export class HorarioDirective {
    @Input() param!: string;

    constructor() {}
}
