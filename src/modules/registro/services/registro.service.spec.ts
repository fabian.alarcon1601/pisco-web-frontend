import { TestBed } from '@angular/core/testing';

import { RegistroService } from './registro.service';

describe('RegistroService', () => {
    let registroService: RegistroService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [RegistroService],
        });
        registroService = TestBed.inject(RegistroService);
    });

    describe('getRegistro$', () => {
        it('should return Observable<Registro>', () => {
            expect(registroService).toBeDefined();
        });
    });
});
