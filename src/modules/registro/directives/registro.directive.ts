import { Directive, Input } from '@angular/core';

@Directive({
    selector: '[sbRegistro]',
})
export class RegistroDirective {
    @Input() param!: string;

    constructor() {}
}
