import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { RegistroService } from '@modules/registro/services';
import {PacienteModel} from '../../../../models/paciente';
import {UsuarioModel} from '../../../../models/usuario';

@Component({
    selector: 'sb-registro-paciente',
    changeDetection: ChangeDetectionStrategy.Default,
    templateUrl: './registro-paciente.component.html',
    styleUrls: ['registro-paciente.component.scss'],
})
export class RegistroPacienteComponent implements OnInit {
    paciente: PacienteModel;
    usuario:UsuarioModel;
    success:boolean;
    error:boolean;
    message:string;
    constructor(private registroService: RegistroService) {
        this.paciente= new PacienteModel();
        this.usuario = new UsuarioModel();
    }
    ngOnInit() {
       
    }
    registrar(form:NgForm,rePass){
        if(rePass==this.usuario.pass){
            this.usuario.rol_id = 1;
            this.registroService.registroUsuario(this.usuario).subscribe((resp:any)=>{
                if( resp.code == 400){
                        this.error=true;
                        this.message='Ups! hubo un error al crear al paciente';
                }
                this.paciente.usuario_id=resp.data.id;
                this.registroService.registroPaciente(this.paciente).subscribe((resp:any)=>{
                    console.log(resp);
                    if(resp.code==400){
                        this.error=true;
                        this.message='Ups! hubo un error al crear al paciente';
                    }
                    if(resp.code == 200){
                        this.success=true;
                        this.message='Se creó el paciente exitosamente';
                    }
                })
            });
        }else{
            this.error=true;
            this.message='las contraseñas no son iguales';
        }
    }
}
