import { RegistroPacienteComponent } from './registro-paciente/registro-paciente.component';
import { RegistroPsicologoComponent } from './registro-psicologo/registro-psicologo.component';

export const components = [RegistroPacienteComponent, RegistroPsicologoComponent];

export * from './registro-paciente/registro-paciente.component';
export * from './registro-psicologo/registro-psicologo.component';
