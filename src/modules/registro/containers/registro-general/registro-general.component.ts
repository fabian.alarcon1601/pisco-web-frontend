import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'sb-registro-general',
    changeDetection: ChangeDetectionStrategy.Default,
    templateUrl: './registro-general.component.html',
    styleUrls: ['registro-general.component.scss'],
})
export class RegistroGeneralComponent implements OnInit {
    paciente:number=1;
    psicologo:number=0;

    constructor(private router:Router) {}
    ngOnInit() {}

    escogerPsicologo() {
        this.psicologo=1;
        this.paciente=0;
    }

    escogerPaciente() {
        this.paciente=1;
        this.psicologo=0;
    }
    inicio(){
        this.router.navigate(['/inicio']);
    }
    login(){
        this.router.navigate(['/login']);
    }
}
