import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FichasClinicasService } from '@modules/fichas-clinicas/services';

@Component({
    selector: 'sb-mostrar-ficha',
    changeDetection: ChangeDetectionStrategy.Default,
    templateUrl: './mostrar-ficha.component.html',
    styleUrls: ['mostrar-ficha.component.scss'],
})
export class MostrarFichaComponent implements OnInit {
    paciente_id;
    ficha_clinica;
    edad;

    constructor(private router:Router,
                private agregarFichaService:FichasClinicasService) {
        const navigation = this.router.getCurrentNavigation();
        this.paciente_id=navigation.extras.state.paciente_id;
        let data = {
            id: this.paciente_id,
        }
        this.agregarFichaService.obtenerFichaClinica(data).subscribe((resp:any)=>{
            this.ficha_clinica= resp.data;
            console.log(this.ficha_clinica);
            let arrayDate = this.ficha_clinica.paciente.fecha_de_nacimiento.split('-');
            let anio = new Date().getFullYear();
            this.edad = anio - arrayDate[0];
        });
    }
    ngOnInit() {
    }
    atras(){
        this.router.navigate(['/fichas/clinicas/ficha/clinica']);
    }
}
