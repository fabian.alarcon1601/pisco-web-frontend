import { FichaClinicaComponent } from './ficha-clinica/ficha-clinica.component';
import { MostrarFichaComponent } from '../../fichas-clinicas/components/mostrar-ficha/mostrar-ficha.component';

export const components = [FichaClinicaComponent, MostrarFichaComponent];

export * from './ficha-clinica/ficha-clinica.component';
export * from '../../fichas-clinicas/components/mostrar-ficha/mostrar-ficha.component';