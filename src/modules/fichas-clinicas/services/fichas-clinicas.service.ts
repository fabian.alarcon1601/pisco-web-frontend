import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import {url} from '../../../environments/environment';

@Injectable()
export class FichasClinicasService {
    private headers = new HttpHeaders(
        {  'Content-Type':  'application/json' }
    );
    constructor(private http:HttpClient) {}

    obtenerListadoPacientes(){
        return this.http.get(url + '/paciente/listar');
    }
    obtenerFichaClinica(data){
        const option = {headers:this.headers};
        console.log(data);
        return this.http.post(url + '/fichas/buscar/paciente', data, option);
    }
    buscarPacientePorNombre(data){
        const option = {headers:this.headers};
        console.log(data);
        return this.http.post(url + '/paciente/buscar/nombre', data, option);
    }

}
