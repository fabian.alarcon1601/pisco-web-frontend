import { FichasClinicasGuard } from './fichas-clinicas.guard';

export const guards = [FichasClinicasGuard];

export * from './fichas-clinicas.guard';
