import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

@Component({
    selector: 'sb-ficha-clinica-container',
    changeDetection: ChangeDetectionStrategy.Default,
    templateUrl: './ficha-clinica.component.html',
    styleUrls: ['ficha-clinica.component.scss'],
})
export class FichaClinicaComponent implements OnInit {
    constructor() {}
    ngOnInit() {}
}
