import { FichaClinicaComponent } from './ficha-clinica/ficha-clinica.component';
import { MostrarFichaComponent } from './mostrar-ficha/mostrar-ficha.component';

export const containers = [FichaClinicaComponent, MostrarFichaComponent];

export * from './ficha-clinica/ficha-clinica.component';
export * from './mostrar-ficha/mostrar-ficha.component'; 