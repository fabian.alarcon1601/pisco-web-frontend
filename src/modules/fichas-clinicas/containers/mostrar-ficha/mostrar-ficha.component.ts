import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

@Component({
    selector: 'sb-mostrar-ficha-container',
    changeDetection: ChangeDetectionStrategy.Default,
    templateUrl: './mostrar-ficha.component.html',
    styleUrls: ['mostrar-ficha.component.scss'],
})
export class MostrarFichaComponent implements OnInit {
    constructor() {}
    ngOnInit() {}
}
