import { Directive, Input } from '@angular/core';

@Directive({
    selector: '[sbFichasClinicas]',
})
export class FichasClinicasDirective {
    @Input() param!: string;

    constructor() {}
}
