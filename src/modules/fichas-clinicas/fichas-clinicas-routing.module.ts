/* tslint:disable: ordered-imports*/
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

/* Module */
import { FichasClinicasModule } from './fichas-clinicas.module';

/* Containers */
import * as fichasClinicasContainers from './containers';

/* Guards */
import * as fichasClinicasGuards from './guards';
import { SBRouteData } from '@modules/navigation/models';

/* Routes */
export const ROUTES: Routes = [
    {
        path: 'ficha/clinica',
        data: {
            title: 'Ficha clinica',
        } as SBRouteData,
        canActivate: [],
        component: fichasClinicasContainers.FichaClinicaComponent,
    },
    {
        path: 'mostrar/ficha',
        data: {
            title: 'Mostrar Ficha',
        } as SBRouteData,
        canActivate: [],
        component: fichasClinicasContainers.MostrarFichaComponent,
    },
];

@NgModule({
    imports: [FichasClinicasModule, RouterModule.forChild(ROUTES)],
    exports: [RouterModule],
})
export class FichasClinicasRoutingModule {}
