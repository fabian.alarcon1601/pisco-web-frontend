/* tslint:disable: ordered-imports*/
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

/* Modules */
import { AppCommonModule } from '@common/app-common.module';
import { NavigationModule } from '@modules/navigation/navigation.module';

/* Components */
import * as loginComponents from './components';

/* Containers */
import * as loginContainers from './containers';

/* Guards */
import * as loginGuards from './guards';

/* Services */
import * as loginServices from './services';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        ReactiveFormsModule,
        FormsModule,
        AppCommonModule,
        NavigationModule,
    ],
    providers: [...loginServices.services, ...loginGuards.guards],
    declarations: [...loginContainers.containers, ...loginComponents.components],
    exports: [...loginContainers.containers, ...loginComponents.components],
})
export class LoginModule {}
