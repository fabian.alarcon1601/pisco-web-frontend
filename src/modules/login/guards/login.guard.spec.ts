import { TestBed } from '@angular/core/testing';

import { LoginGuard } from './login.guard';

describe('Login Guards', () => {
    let loginGuard: LoginGuard;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [LoginGuard],
        });
        loginGuard = TestBed.inject(LoginGuard);
    });

    describe('canActivate', () => {
        it('should return an Observable<boolean>', () => {
            loginGuard.canActivate().subscribe(response => {
                expect(response).toEqual(true);
            });
        });
    });

});
