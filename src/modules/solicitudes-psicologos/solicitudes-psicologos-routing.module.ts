/* tslint:disable: ordered-imports*/
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

/* Module */
import { SolicitudesPsicologosModule } from './solicitudes-psicologos.module';

/* Containers */
import * as solicitudesPsicologosContainers from './containers';

/* Guards */
import * as solicitudesPsicologosGuards from './guards';
import { SBRouteData } from '@modules/navigation/models';
import { ListadoSolicitudesComponent } from './components';

/* Routes */
export const ROUTES: Routes = [
    {
        path: 'listado/solicitudes',
        data: {
            title: 'Listado Solicitudes',
        } as SBRouteData,
        canActivate: [],
        component: solicitudesPsicologosContainers.ListadoSolicitudesComponent,
    },
    {
        path: 'solicitudes/aprobadas',
        data: {
            title: 'Solicitudes aprobadas',
        } as SBRouteData,
        canActivate: [],
        component: solicitudesPsicologosContainers.SolicitudesAprobadasComponent,
    },
    {
        path: 'solicitudes/rechazadas',
        data: {
            title: 'Solicitudes rechazadas',
        } as SBRouteData,
        canActivate: [],
        component: solicitudesPsicologosContainers.SolicitudesRechazadasComponent,
    },
];

@NgModule({
    imports: [SolicitudesPsicologosModule, RouterModule.forChild(ROUTES)],
    exports: [RouterModule],
})
export class SolicitudesPsicologosRoutingModule {}
