import { TestBed } from '@angular/core/testing';

import { SolicitudesPsicologosGuard } from './solicitudes-psicologos.guard';

describe('SolicitudesPsicologos Guards', () => {
    let solicitudesPsicologosGuard: SolicitudesPsicologosGuard;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [SolicitudesPsicologosGuard],
        });
        solicitudesPsicologosGuard = TestBed.inject(SolicitudesPsicologosGuard);
    });

    describe('canActivate', () => {
        it('should return an Observable<boolean>', () => {
            solicitudesPsicologosGuard.canActivate().subscribe(response => {
                expect(response).toEqual(true);
            });
        });
    });

});
