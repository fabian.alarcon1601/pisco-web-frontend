import { SolicitudesPsicologosGuard } from './solicitudes-psicologos.guard';

export const guards = [SolicitudesPsicologosGuard];

export * from './solicitudes-psicologos.guard';
