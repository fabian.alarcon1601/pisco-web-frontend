import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

@Component({
    selector: 'sb-solicitudes-rechazadas-container',
    changeDetection: ChangeDetectionStrategy.Default,
    templateUrl: './solicitudes-rechazadas.component.html',
    styleUrls: ['solicitudes-rechazadas.component.scss'],
})
export class SolicitudesRechazadasComponent implements OnInit {
    constructor() {

    }
    ngOnInit() {
    }
}
