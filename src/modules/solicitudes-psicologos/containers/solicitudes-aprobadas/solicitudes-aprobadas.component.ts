import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

@Component({
    selector: 'sb-solicitudes-aprobadas-container',
    changeDetection: ChangeDetectionStrategy.Default,
    templateUrl: './solicitudes-aprobadas.component.html',
    styleUrls: ['solicitudes-aprobadas.component.scss'],
})
export class SolicitudesAprobadasComponent implements OnInit {
    constructor() {}
    ngOnInit() {}
}
