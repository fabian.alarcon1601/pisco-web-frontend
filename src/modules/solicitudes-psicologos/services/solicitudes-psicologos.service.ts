import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { url } from '../../../environments/environment';

@Injectable()
export class SolicitudesPsicologosService {
    private headers = new HttpHeaders(
        {  'Content-Type':  'application/json' }
      );
    constructor(private http:HttpClient) {}

    obtenerListadoPsicologoPendientes(){
        return this.http.get(url + '/profesional/listar');
    }
    obtenerListadoPsicologosAprobados(){
        return this.http.get(url + '/profesional/listar/aprobado');
    }
    obtenerListadoPsicologosRechazado(){
        return this.http.get(url + '/profesional/listar/rechazado');
    }
    aprobarPsicologo(data){
        const option = {headers:this.headers};
        console.log(data);
        return this.http.post(url + '/profesional/actualizacion/estado',data,option);
    }

}
