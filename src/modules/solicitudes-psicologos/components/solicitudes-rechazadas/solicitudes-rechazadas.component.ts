import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { SolicitudesPsicologosService } from '@modules/solicitudes-psicologos/services';

@Component({
    selector: 'sb-solicitudes-rechazadas',
    changeDetection: ChangeDetectionStrategy.Default,
    templateUrl: './solicitudes-rechazadas.component.html',
    styleUrls: ['solicitudes-rechazadas.component.scss'],
})
export class SolicitudesRechazadasComponent implements OnInit {
    psicologos;
    constructor(private solicitudesPsicologosService:SolicitudesPsicologosService) {

    }
    ngOnInit() {
         this.solicitudesPsicologosService.obtenerListadoPsicologosRechazado().subscribe((resp:any)=>{
            this.psicologos = resp.profesionales;
        })
    }
}
