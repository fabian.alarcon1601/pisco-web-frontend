import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { SolicitudesPsicologosService } from '@modules/solicitudes-psicologos/services';

@Component({
    selector: 'sb-solicitudes-aprobadas',
    changeDetection: ChangeDetectionStrategy.Default,
    templateUrl: './solicitudes-aprobadas.component.html',
    styleUrls: ['solicitudes-aprobadas.component.scss'],
})
export class SolicitudesAprobadasComponent implements OnInit {
    psicologos;
    constructor(private solicitudesPsicologoService:SolicitudesPsicologosService) {}
    ngOnInit() {
        this.solicitudesPsicologoService.obtenerListadoPsicologosAprobados().subscribe((resp:any)=>{
            console.log(resp);
            this.psicologos=resp.profesionales;
        });
    }
}
