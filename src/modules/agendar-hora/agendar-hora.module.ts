/* tslint:disable: ordered-imports*/
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

/* Modules */
import { AppCommonModule } from '@common/app-common.module';
import { NavigationModule } from '@modules/navigation/navigation.module';

/* Components */
import * as agendarHoraComponents from './components';

/* Containers */
import * as agendarHoraContainers from './containers';

/* Guards */
import * as agendarHoraGuards from './guards';

/* Services */
import * as agendarHoraServices from './services';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        ReactiveFormsModule,
        FormsModule,
        AppCommonModule,
        NavigationModule,
    ],
    providers: [...agendarHoraServices.services, ...agendarHoraGuards.guards],
    declarations: [...agendarHoraContainers.containers, ...agendarHoraComponents.components],
    exports: [...agendarHoraContainers.containers, ...agendarHoraComponents.components],
})
export class AgendarHoraModule {}
