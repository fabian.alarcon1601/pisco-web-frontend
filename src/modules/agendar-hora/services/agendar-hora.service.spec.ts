import { TestBed } from '@angular/core/testing';

import { AgendarHoraService } from './agendar-hora.service';

describe('AgendarHoraService', () => {
    let agendarHoraService: AgendarHoraService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [AgendarHoraService],
        });
        agendarHoraService = TestBed.inject(AgendarHoraService);
    });

    describe('getAgendarHora$', () => {
        it('should return Observable<AgendarHora>', () => {
            expect(agendarHoraService).toBeDefined();
        });
    });
});
