import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

@Component({
    selector: 'sb-buscar-profesional-container',
    changeDetection: ChangeDetectionStrategy.Default,
    templateUrl: './buscar-profesional.component.html',
    styleUrls: ['buscar-profesional.component.scss'],
})
export class BuscarProfesionalComponent implements OnInit {
    constructor() {}
    ngOnInit() {}
}
