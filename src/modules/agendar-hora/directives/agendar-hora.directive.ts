import { Directive, Input } from '@angular/core';

@Directive({
    selector: '[sbAgendarHora]',
})
export class AgendarHoraDirective {
    @Input() param!: string;

    constructor() {}
}
