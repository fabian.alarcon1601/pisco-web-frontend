import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProfesionalModel } from 'models/profesional';

@Component({
    selector: 'sb-agendar',
    changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './agendar.component.html',
    styleUrls: ['agendar.component.scss'],
})
export class AgendarComponent implements OnInit {
    constructor(private router:Router) {}
    ngOnInit() {}

    buscarProfesional(){
        this.router.navigate(['agendar/hora/buscar/profesional']);
    }

}
