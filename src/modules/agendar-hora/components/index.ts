import { BuscarProfesionalComponent } from './buscar-profesional/buscar-profesional.component';
import { AgendarComponent } from './agendar/agendar.component';

export const components = [BuscarProfesionalComponent, AgendarComponent];

export * from './buscar-profesional/buscar-profesional.component';
export * from './agendar/agendar.component';
